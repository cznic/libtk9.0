// Copyright 2024 The libtk9_0-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package libtk9_0 // import "modernc.org/libtk9_0"

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"testing"

	_ "github.com/adrg/xdg"
	"github.com/evilsocket/islazy/zip"
	_ "modernc.org/ccgo/v4/lib"
	util "modernc.org/fileutil/ccgo"
	"modernc.org/libtcl9.0/library"
)

var (
	isBuilder = os.Getenv("MODERNC_BUILDER") != ""
	goos      = runtime.GOOS
	goarch    = runtime.GOARCH
	target    = fmt.Sprintf("%s/%s", goos, goarch)
	oXTags    = flag.String("xtags", "", "passed to go build of tcltest")
)

func TestMain(m *testing.M) {
	runtime.LockOSThread()
	flag.Parse()
	rc := m.Run()
	os.Exit(rc)
}

func Test(t *testing.T) {
	if isBuilder {
		t.Skip("SKIP: not running in builder")
	}

	tempDir := t.TempDir()
	fn := "tktest"
	if runtime.GOOS == "windows" {
		fn += ".exe"
	}
	bin := filepath.Join(tempDir, fn)
	if out, err := util.Shell(nil, "go", "build", "-o", bin, "-tags="+*oXTags, "./internal/tktest"); err != nil {
		t.Fatalf("%v\n%s", err, out)
	}

	fn = filepath.Join(tempDir, "library.zip")
	if err := os.WriteFile(fn, []byte(library.Zip), 0600); err != nil {
		t.Fatal(err)
	}

	if _, err := zip.Unzip(fn, tempDir); err != nil {
		t.Fatal(err)
	}

	os.Setenv("ERROR_ON_FAILURES", "1")
	os.Setenv("LC_ALL", "C.UTF-8")
	os.Setenv("TCL_LIBRARY", filepath.Join(tempDir, "library"))
	os.Setenv("TK_LIBRARY", "library")
	notFile := []string{"__none__"}
	skip := []string{"__none__"}
	switch target {
	case "darwin/amd64":
		notFile = []string{
			"entry.test",
			"spinbox.test",
		}
		skip = []string{
			//TODO
			"canvText-6.1",
			"canvText-6.5",
			"canvText-6.9",
			"clipboard-6.2",
			"font-21.6",
			"spinbox-20.5",
			"spinbox-20.7",
			"textDisp-11.12",
			"textDisp-11.2",
			"textDisp-11.4",
			"textDisp-11.8",
			"textDisp-11.9",
			"textDisp-4.14",
			"textDisp-4.8",
			"textDisp-6.1",
			"textDisp-8.7",
			"textDisp-8.9",
			"unixfont-1.2",
			"unixfont-2.2",
			"unixfont-2.3",
			"unixfont-2.6",
			"unixfont-2.8",
			"unixfont-2.9",
			"unixfont-5.12",
			"unixfont-5.8",
			"unixfont-5.9",
			"unixfont-8.5",
			"unixSelect-1.*",
		}
	case "darwin/arm64":
		skip = []string{
			//TODO
			"clipboard-6.2",
			"font-21.6",
			"spinbox-20.5",
			"spinbox-20.7",
			"textDisp-11.12",
			"textDisp-11.2",
			"textDisp-11.4",
			"textDisp-11.8",
			"textDisp-11.9",
			"textDisp-4.14",
			"textDisp-4.8",
			"textDisp-6.1",
			"textDisp-8.7",
			"textDisp-8.9",
			"unixfont-1.2",
			"unixfont-2.2",
			"unixfont-2.3",
			"unixfont-2.6",
			"unixfont-2.8",
			"unixfont-2.9",
			"unixfont-5.12",
			"unixfont-5.8",
			"unixfont-5.9",
			"unixfont-8.5",
			"unixSelect-1.*",
		}
	case "linux/amd64":
		skip = []string{
			//TODO
			"focus-2.10",
			"focus-2.15",
			"focus-2.16",
			"focus-2.17",
			"focus-2.6",
			"focus-2.7",
			"focus-2.9",
			"focus-5.1",
			"font-10.9",
			"font-21.6",
			"font-4.9",
			"font-45.3",
			"font-8.4",
			"frame-12.3",
			"send-8.16",
			"textImage-4.2",
			"unixWm-4.1",
			"unixWm-4.2",
			"unixWm-4.3",
			"unixWm-51.7",
			"unixfont-1.2",
			"unixfont-2.2",
			"unixfont-2.3",
			"unixfont-2.4",
			"unixfont-2.6",
			"unixfont-2.8",
			"unixfont-2.9",
			"unixfont-5.12",
			"unixfont-5.8",
			"unixfont-5.9",
			"unixfont-8.4",
			"unixfont-8.6",
			"unixfont-9.1",
			"unixfont-9.2",
		}
	case "linux/386":
		skip = []string{
			//TODO
			"focus-2.10",
			"focus-2.15",
			"focus-2.16",
			"focus-2.17",
			"focus-2.6",
			"focus-2.7",
			"focus-2.9",
			"focus-5.1",
			"font-10.9",
			"font-21.6",
			"font-4.9",
			"font-8.4",
			"send-8.16",
			"unixWm-4.1",
			"unixWm-4.2",
			"unixWm-4.3",
			"unixWm-51.7",
			"unixfont-1.2",
			"unixfont-2.2",
			"unixfont-2.3",
			"unixfont-2.4",
			"unixfont-2.6",
			"unixfont-2.8",
			"unixfont-2.9",
			"unixfont-5.12",
			"unixfont-5.8",
			"unixfont-5.9",
			"unixfont-8.4",
			"unixfont-8.6",
			"unixfont-9.1",
			"unixfont-9.2",
			"winfo-13.4",
		}
	case "linux/arm64":
		skip = []string{
			//TODO
			"focus-2.10",
			"focus-2.15",
			"focus-2.16",
			"focus-2.17",
			"focus-2.6",
			"focus-2.7",
			"focus-2.9",
			"focus-5.1",
			"font-21.6",
			"font-38.1",
			"font-38.2",
			"font-38.3",
			"font-38.5",
			"font-38.6",
			"font-4.9",
			"font-40.1",
			"font-40.2",
			"font-40.3",
			"font-40.5",
			"font-43.1",
			"textImage-4.3",
			"unixWm-4.1",
			"unixWm-4.2",
			"unixWm-4.3",
			"unixWm-51.7",
			"unixfont-2.2",
			"unixfont-2.3",
			"unixfont-2.4",
			"unixfont-2.9",
			"unixfont-8.4",
			"unixfont-8.6",
			"unixfont-9.1",
			"unixfont-9.2",
			"winfo-13.4",
		}
	}
	t.Run("tk", func(t *testing.T) { test(t, bin, filepath.Join("internal", "tests", "all.tcl"), notFile, skip) })
	notFile = []string{"__none__"}
	skip = []string{"__none__"}
	switch target {
	case "linux/amd64":
		// nop
	case "linux/arm64":
		// nop
	}
	t.Run("ttk", func(t *testing.T) { test(t, bin, filepath.Join("internal", "tests", "ttk", "all.tcl"), notFile, skip) })
}

func test(t *testing.T, bin, file string, notFile, skip []string) {
	out, err := util.Shell(
		nil,
		bin,
		file,
		"-geometry", "+0+0",
		"-notfile", strings.Join(notFile, " "),
		"-skip", strings.Join(skip, " "),
	)
	if err != nil {
		t.Error(err)
		return
	}

	alls := string(out)
	all := strings.Split(alls, "\n")
	if !strings.Contains(alls, "Total") ||
		!strings.Contains(alls, "Passed") ||
		!strings.Contains(alls, "Skipped") ||
		!strings.Contains(alls, "Failed") {
		t.Errorf("final summary not detected (test crashed?)")
		return
	}
out:
	for i, v := range all {
		switch {
		case
			strings.Contains(v, "Test file error"),
			strings.Contains(v, "panic:"),
			strings.HasPrefix(v, "====") && strings.Contains(v, "FAILED"):

			t.Error(v)
		case strings.HasPrefix(v, "all.tcl:"):
			t.Logf("\n%s", strings.Join(all[i:], "\n"))
			break out
		}
	}
}
