// Copyright 2024 The libtk9_0-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:generate go run generator.go

// Package libtk9_0 is a ccgo/v4 version the Tk GUI toolkit.
package libtk9_0 // import "modernc.org/libtk9_0"

const (
	Version = "tk9.0.1"
)
