// Copyright 2024 The libtk9_0-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

//go:build ignore
// +build ignore

package main

import (
	"bytes"
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"strings"

	"github.com/adrg/xdg"
	"modernc.org/cc/v4"
	ccgo "modernc.org/ccgo/v4/lib"
	util "modernc.org/fileutil/ccgo"
	"modernc.org/libtcl9.0"
)

const (
	archivePath = "tk" + version + "-src.tar.gz"
	version     = "9.0.1"
)

var (
	goarch        = env("TARGET_GOARCH", env("GOARCH", runtime.GOARCH))
	goos          = env("TARGET_GOOS", env("GOOS", runtime.GOOS))
	target        = fmt.Sprintf("%s/%s", goos, goarch)
	sed           = "sed"
	j             = fmt.Sprint(runtime.GOMAXPROCS(-1))
	xdgConfigHome = xdg.ConfigHome
	libraryGo     = `// Copyright 2024 The libtk9_0-go Authors. All rights reserved.
// Use of the source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Package library provides the Tk standard library.
package library // import "modernc.org/libtk9_0/library"

import (
	_ "embed"
)

// Zip contains the Tk standard library archive.
//
//go:embed library.zip
var Zip string
`
)

func fail(rc int, msg string, args ...any) {
	fmt.Fprintln(os.Stderr, strings.TrimSpace(fmt.Sprintf(msg, args...)))
	os.Exit(rc)
}

func main() {
	if ccgo.IsExecEnv() {
		if err := ccgo.NewTask(goos, goarch, os.Args, os.Stdout, os.Stderr, nil).Main(); err != nil {
			fmt.Fprintln(os.Stderr, err)
		}
		return
	}

	dev := os.Getenv("GO_GENERATE_DEV") != ""
	f, err := os.Open(archivePath)
	if err != nil {
		fail(1, "cannot open tar file: %v\n", err)
	}

	xdgDir := filepath.Join(xdgConfigHome, "ccgo", "v4", "libtk9.0", goos, goarch)
	libRoot := filepath.Join(xdgDir, "tk"+version)
	os.RemoveAll(libRoot)
	makeRoot := filepath.Join(libRoot, "unix")
	os.MkdirAll(xdgDir, 0770)
	tkDocs := filepath.Join(libRoot, "doc")
	fmt.Fprintf(os.Stderr, "xdgDir %s\n", xdgDir)
	fmt.Fprintf(os.Stderr, "archivePath %s\n", archivePath)
	fmt.Fprintf(os.Stderr, "libRoot %s\n", libRoot)
	fmt.Fprintf(os.Stderr, "makeRoot %s\n", makeRoot)
	fmt.Fprintf(os.Stderr, "tkDocs %s\n", tkDocs)
	if target == "linux/386" {
		os.RemoveAll(filepath.Join("library"))
	}
	os.RemoveAll(filepath.Join("include", goos, goarch))
	util.MustShell(true, nil, "sh", "-c", fmt.Sprintf("find %s -name *%s_%s.go -delete", filepath.Join("internal", "tests"), goos, goarch))
	util.MustUntar(true, xdgDir, f, nil)
	patches(filepath.Join(libRoot))
	if os.Getenv("GO_GENERATE_API_DEV") != "" {
		return
	}

	util.MustCopyFile(true, "LICENSE-TCL", filepath.Join(libRoot, "license.terms"), nil)
	if err != nil {
		fail(1, "%s\n", err)
	}
	result := "libtk.a.go"
	cwd := util.MustAbsCwd(true)
	tclConfig := filepath.Join(xdgConfigHome, "ccgo", "v4", "libtcl9.0", goos, goarch, libtcl9_0.Version, "unix")
	ilibfontconfig := filepath.Join(cwd, "..", "libfontconfig", "include", goos, goarch)
	ilibtcl := filepath.Join(xdgConfigHome, "ccgo", "v4", "libtcl9.0", goos, goarch, libtcl9_0.Version, "generic")
	ilibx11 := filepath.Join(cwd, "..", "libX11", "include", goos, goarch)
	ilibxft := filepath.Join(cwd, "..", "libXft", "include", goos, goarch)
	args := []string{os.Args[0]}
	util.MustInDir(true, makeRoot, func() (err error) {
		defer func() {
			if err != nil {
				trc("FAIL err=%v (%v: %v:)", err, origin(1), origin(2))
			}
			if e := recover(); e != nil {
				trc("PANIC: FAIL e=%v", e)
				panic(e)
			}
		}()
		cflags := []string{
			"-DNDEBUG",
			"-I", ilibtcl,
		}
		if s := cc.LongDouble64Flag(goos, goarch); s != "" {
			cflags = append(cflags, s)
		}
		util.MustShell(true, nil, "sh", "-c", fmt.Sprintf("go mod init example.com/libtk9.0 ; go get %s",
			strings.Join([]string{
				"modernc.org/libX11@latest",
				"modernc.org/libXft@latest",
				"modernc.org/libc@latest",
				"modernc.org/libfontconfig@latest",
				"modernc.org/libtcl9.0@latest",
				"modernc.org/libtk9.0@latest",
			}, " ")))
		if dev {
			util.MustShell(true, nil, "sh", "-c", fmt.Sprintf("go work init ; go work use %s",
				strings.Join([]string{
					"$GOPATH/src/modernc.org/libX11",
					"$GOPATH/src/modernc.org/libXft",
					"$GOPATH/src/modernc.org/libc",
					"$GOPATH/src/modernc.org/libfontconfig",
					"$GOPATH/src/modernc.org/libtcl9.0",
					"$GOPATH/src/modernc.org/libtk9.0",
				}, " ")))
		}
		util.MustShell(true, nil, "sh", "-c", fmt.Sprintf("CFLAGS='%s' ./configure "+
			"--disable-libcups "+ //TODO
			"--disable-shared "+
			"--disable-xss "+
			"--enable-64bit "+
			"--x-includes=/usr/include "+
			fmt.Sprintf("--with-tcl=%q ", tclConfig)+
			"", strings.Join(cflags, " ")))
		if dev {
			args = append(
				args,
				"-absolute-paths",
				"-positions",
			)
		}
		args = append(args,
			"--prefix-external=X",
			"--prefix-field=F",
			"--prefix-static-internal=_",
			"--prefix-static-none=_",
			"--prefix-tagged-struct=T",
			"--prefix-tagged-union=T",
			"--prefix-typename=T",
			"--prefix-undefined=_",
			"-eval-all-macros",
			"-extended-errors",
			"-ignore-link-errors",
			"-ignore-unsupported-alignment",
			"-I", ilibfontconfig,
			"-I", ilibtcl,
			"-I", ilibx11,
			"-I", ilibxft,
			"-lX11",
			"-lXft",
			"-lfontconfig",
			"-ltcl9.0",
		)
		ccgo.NewTask(goos, goarch, append(args, "-exec", "make", "-j", j, "binaries", "tktest"), os.Stdout, os.Stderr, nil).Exec()
		return ccgo.NewTask(goos, goarch, append(args, "-o", result, "--package-name", "libtk9_0", "libtcl9tk9.0.a"), os.Stdout, os.Stderr, nil).Main()
	})

	util.MustCopyDir(true, filepath.Join("internal", "tests"), filepath.Join(libRoot, "tests"), nil)
	util.MustCopyDir(true, "library", filepath.Join(libRoot, "library"), nil)
	if target == "linux/386" {
		util.MustInDir(true, libRoot, func() (err error) {
			_, err = util.Shell(nil, "zip", "-r", filepath.Join(cwd, "library", "library.zip"), "library")
			return err
		})
	}
	if err := os.WriteFile(filepath.Join("library", "library.go"), []byte(libraryGo), 0660); err != nil {
		fail(1, "%s\n", err)
	}

	fn := fmt.Sprintf("libtk9_0_%s_%s.go", goos, goarch)
	util.MustCopyFile(true, fn, filepath.Join(makeRoot, result), nil)
	fn = fmt.Sprintf("tktest_%s_%s.go", goos, goarch)
	util.MustCopyFile(true, filepath.Join("internal", "tktest", fn), filepath.Join(makeRoot, "tktest.go"), nil)
	util.Shell(nil, "git", "status")
}

type patch struct {
	path string
	old  string
	new  string
}

func patches(dir string) {
	for _, v := range []patch{} {
		fn := filepath.Join(dir, filepath.FromSlash(v.path))
		b, err := os.ReadFile(fn)
		ln := len(b)
		if err != nil {
			fail(1, "reading %v: %v", fn, err)
		}

		b = bytes.Replace(b, []byte(v.old), []byte(v.new), 1)
		if len(v.old) != len(v.new) && len(b) == ln {
			fail(1, "patch not found %q", v)
		}

		if err := os.WriteFile(fn, b, 0660); err != nil {
			fail(1, "writing %v: %v", fn, err)
		}
	}
}

func env(name, deflt string) (r string) {
	r = deflt
	if s := os.Getenv(name); s != "" {
		r = s
	}
	return r
}

// origin returns caller's short position, skipping skip frames.
//
//lint:ignore U1000 debug helper
func origin(skip int) string {
	pc, fn, fl, _ := runtime.Caller(skip)
	f := runtime.FuncForPC(pc)
	var fns string
	if f != nil {
		fns = f.Name()
		if x := strings.LastIndex(fns, "."); x > 0 {
			fns = fns[x+1:]
		}
		if strings.HasPrefix(fns, "func") {
			num := true
			for _, c := range fns[len("func"):] {
				if c < '0' || c > '9' {
					num = false
					break
				}
			}
			if num {
				return origin(skip + 2)
			}
		}
	}
	return fmt.Sprintf("%s:%d:%s", filepath.Base(fn), fl, fns)
}

// todo prints and return caller's position and an optional message tagged with TODO. Output goes to stderr.
//
//lint:ignore U1000 debug helper
func todo(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s\n\tTODO %s", origin(2), s)
	// fmt.Fprintf(os.Stderr, "%s\n", r)
	// os.Stdout.Sync()
	return r
}

// trc prints and return caller's position and an optional message tagged with TRC. Output goes to stderr.
//
//lint:ignore U1000 debug helper
func trc(s string, args ...interface{}) string {
	switch {
	case s == "":
		s = fmt.Sprintf(strings.Repeat("%v ", len(args)), args...)
	default:
		s = fmt.Sprintf(s, args...)
	}
	r := fmt.Sprintf("%s: TRC %s", origin(2), s)
	fmt.Fprintf(os.Stderr, "%s\n", r)
	os.Stderr.Sync()
	return r
}
